from django.db import models


class Link(models.Model):
    url = models.URLField(max_length=2048)  # 2kb
    short = models.CharField(max_length=10, null=True, blank=True)
    created = models.DateTimeField(auto_now=True)


class Log(models.Model):
    """
    Simple log model used for analytics
    """
    url = models.URLField(max_length=2048)  # 2kb
    short = models.CharField(max_length=10)
    domain = models.CharField(max_length=255)
    visited_at = models.DateTimeField(auto_now=True)
