import json
from http import HTTPStatus

from django.conf import settings
from django.core.exceptions import ValidationError
from django.http import HttpResponse, JsonResponse, HttpResponsePermanentRedirect
from django.template.loader import render_to_string
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST, require_GET

from .service import Shortener, Analytics


def index(request):
    """
    Simple UI just for fun
    :param request:
    :return:
    """
    return HttpResponse(render_to_string('index.html', request=request))


def redirect(request, short):
    """
    Function takes short url slug and redirects to long url
    :param request:
    :param short:
    :return:
    """
    if not (settings.SHORTENER_MIN_LEN <= len(short) <= settings.SHORTENER_MAX_LEN):
        return HttpResponse("incorrect link length", status=HTTPStatus.BAD_REQUEST)

    to = Shortener.find_link(short)
    if to:
        Analytics.log(url=to, short=short)
        return HttpResponsePermanentRedirect(to)
    else:
        return HttpResponse(status=HTTPStatus.NOT_FOUND)


@csrf_exempt
@require_POST
def shorten(request):
    """
    An endpoint that receives a URL and returns a new shortened URL
    """

    try:
        data = json.loads(request.body)
        url = data.get('url', None)
    except (json.JSONDecodeError, AttributeError):
        return JsonResponse({'error': 'Url is not specified'}, status=HTTPStatus.BAD_REQUEST)

    try:
        res = Shortener.generate_short(url)

        shorten_url = f'{request.scheme}://{request.get_host()}/{res}'

        return JsonResponse(dict(shorten=shorten_url))
    except ValidationError:
        return JsonResponse(dict(error='Unable to shorten that link. It is not a valid url.'), status=HTTPStatus.BAD_REQUEST)


@require_GET
def recent(request):
    """
    An endpoint to retrieve the last 100 shortened URLs
    """
    res = [{'url': link.url, 'short': link.short} for link in Analytics.recent()]
    return JsonResponse(res, safe=False)


@require_GET
def top(request):
    """
    An endpoint to retrieve the top 10 most popular shortened domains in the past month
    """
    res = [{'domain': link['domain'], 'count': link['total']} for link in Analytics.top_for_last_month()]

    return JsonResponse(res, safe=False)


@require_GET
def count(request, short):
    """
    An endpoint to retrieve the number of times a shortened URL has been visited.
    """
    return JsonResponse({'count': Analytics.count(short)})
