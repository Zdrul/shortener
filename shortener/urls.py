from django.urls import path

from .views import shorten, recent, top, count, index, redirect


urlpatterns = [
    path('', index),
    path('shorten/', shorten),
    path('recent/', recent),
    path('top/', top),
    path('count/<str:short>/', count),
    path('<str:short>/', redirect),
]
