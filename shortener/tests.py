from urllib.parse import urlparse

import pytest
from django.conf import settings
from django.core.exceptions import ValidationError
from django.test import Client

from shortener.service import Shortener


@pytest.mark.django_db(transaction=True)
class TestsWeb:

    client = Client()

    @pytest.mark.parametrize("data, status", [
        (None, 400),
        (dict(url='some-invalid-url'), 400),
        (dict(url=''), 400),
        (dict(url='https://test.com'), 200),
    ])
    def test_shorten(self, data, status):
        response = self.client.post('/shorten/', data=data, content_type='application/json')

        assert response.status_code == status

        if response.status_code == 200:
            assert response.json().get('shorten') is not None

    def test_recent(self):
        recent_size = 5
        for i in range(recent_size):
            self.client.post('/shorten/', data=dict(url=f'https://google.com/{i}'), content_type='application/json')

        response = self.client.get('/recent/')

        assert response.status_code == 200
        for i, link in enumerate(response.json()):
            assert link.get('url') == f'https://google.com/{recent_size - (i + 1)}'

    def test_top(self):
        top_size = 5
        response = self.client.post('/shorten/', data=dict(url=f'https://google.com/'), content_type='application/json')
        for i in range(top_size):
            res = self.client.get(response.json().get('shorten')+'/')
            assert res.status_code == 301

        response = self.client.get('/top/')
        assert response.status_code == 200
        assert response.json()[0] == {'domain': 'google.com', 'count': 5}

    def test_count(self):
        response = self.client.post('/shorten/', data=dict(url='https://google.com/'), content_type='application/json')
        shorten = response.json().get('shorten')
        url = urlparse(shorten)
        visit_times = 5

        for i in range(visit_times):
            res = self.client.get(url.path+'/')
            assert res.status_code == 301

        response = self.client.get(f'/count{url.path}/')

        assert response.status_code == 200
        assert response.json().get('count') == visit_times


@pytest.mark.django_db
class TestsIntegration:

    def test_invalid_shorten(self):
        with pytest.raises(ValidationError):
            Shortener.generate_short('invalid')

    def test_hardcoded_shorten(self):
        expected = {
            1: settings.ALPHABET.base[0] * 3 + settings.ALPHABET.base[1],
            63: (settings.ALPHABET.base[0] + settings.ALPHABET.base[1]) * 2,
            125: settings.ALPHABET.base[0] + settings.ALPHABET.base[2] + settings.ALPHABET.base[0] + settings.ALPHABET.base[1],

        }
        for i in range(1, settings.ALPHABET.length * 2 + 2):
            shorten = Shortener.generate_short(f'https://google.com/{i}')
            if i in expected:
                assert shorten == expected[i]

    def test_there_and_back(self):
        for i in range(1, settings.ALPHABET.length + 2):
            long = f'https://google.com/{i}'
            shorten = Shortener.generate_short(f'https://google.com/{i}')
            assert long == Shortener.find_link(shorten)
