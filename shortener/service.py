from functools import lru_cache
from typing import List, Dict, Optional
from urllib.parse import urlparse

from django.conf import settings
from django.db.models import Count
from django.utils import timezone

from .models import Link, Log

ALPHABET = settings.ALPHABET


class Shortener:

    @classmethod
    @lru_cache(maxsize=None)
    def find_link(cls, short: str) -> Optional[Log]:
        try:
            link_id = cls._base62_to_int(short)
            return Link.objects.get(id=link_id).url
        except Link.DoesNotExist:
            return None

    @classmethod
    def generate_short(cls, url: str) -> str:
        """
        Short url slug generates by converting integer record id to a base62 string
        :param url:
        :return:
        """
        link = Link(url=url, short=None)
        link.clean_fields()
        link.save()
        short = cls._int_to_base62(link.id)

        # it's not necessary to save short, but useful for analytics
        # to increase performance we can avoid it
        link.short = short
        link.save(update_fields=['short'])

        return short

    @classmethod
    def _int_to_base62(cls, link_id: int) -> str:
        link_id += settings.SHORTENER_MIN_ID
        res = ''

        while link_id > 0:
            res += ALPHABET.base[link_id % ALPHABET.length]
            link_id //= ALPHABET.length

        return res

    @classmethod
    def _base62_to_int(cls, short: str) -> int:
        res = 0

        for i, char in enumerate(short):
            res += ALPHABET.length ** i * ALPHABET.map.get(char)
        res = res - settings.SHORTENER_MIN_ID

        return res


class Analytics:

    @classmethod
    def log(cls, url, short):
        """
        Just save redirect to log
        """
        Log.objects.create(
            url=url,
            short=short,
            domain=urlparse(url).netloc,
        )

    @classmethod
    def recent(cls, count=100) -> List[Log]:
        return Link.objects.all().order_by('-id')[:count]

    @classmethod
    def top_for_last_month(cls, count=10) -> List[Dict]:
        """
        Get most popular domains for the past month
        """
        last_month = timezone.now().replace(day=1, hour=0, minute=0, second=0)

        return Log.objects.filter(visited_at__gte=last_month)\
                  .values('domain').annotate(total=Count('domain'))\
                  .order_by('-total')[:count]

    @classmethod
    def count(cls, short: str) -> int:
        """
        How many times short url has been visited
        """
        return Log.objects.filter(short=short).count()
